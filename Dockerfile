FROM maven

WORKDIR /usr/src/back

COPY . ./

RUN mvn install -DskipTests

CMD ["mvn", "spring-boot:run"]